# Hello-express
### Table of content
- [Hello-express](#hello-express)
    - [Table of content](#table-of-content)
  - [Getting started](#getting-started)
    - [Funny cat picture to brighten the day!](#funny-cat-picture-to-brighten-the-day)


This project contains REST API with following features:

- Welcome endpoint
- Multiply endpoint

## Getting started

First: make sure you have NodeJS installed.
```sh
node --version
```

If this does not return version number head to this link and install NodeJS

[http://nodejs.org](https://nodejs.org/en)

Second: install the dependencies
```sh
npm i
```

Third: Start the REST API service:

`app.js` with following command 
```sh
node app.js
```

Or in the latest NodeJS version (>=20):

```sh
node --watch app.js
```


### Funny cat picture to brighten the day!

![cat](https://untamed.com/cdn/shop/articles/can_cats_eat_bread_featured_1200x1200.png?v=1640628523)

